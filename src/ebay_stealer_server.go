package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	//	"os"
	//	"io"
	//	"bytes"
	//	"html"
	//	"html/template"
	"fmt"
	"stealerPkg"
	"strconv"
)
// Binding from form values
type StealerForm struct {
	Add_item_verb		string 	`form:"add_item_verb" 	binding:"required"`
	Items_per_page		int 	`form:"items_per_page" 	binding:"required"`
	Page_number			int 	`form:"page_number" 	binding:"required"`
	Sort 				int 	`form:"sort" 			binding:"required"`
	Prod_user_id 		string 	`form:"prod_user_id" 	binding:"required"`
	Site_id				string 	`form:"site_id" 		binding:"required"`
	Paypal				string 	`form:"paypal" 			binding:"required"`
	Prod_ebay_token		string 	`form:"prod_ebay_token" binding:"required"`
	Sand_ebay_token		string 	`form:"sand_ebay_token" binding:"required"`
}

func main() {
	r := gin.Default() //gin.New()
	r.LoadHTMLTemplates("form.html")

	//http://localhost:8080/form
	r.GET("/form", func(c *gin.Context) {
			obj := gin.H{"title": "Main form"}
			//draw html form
			c.HTML(200, "form.html", obj)
		})
	//http://localhost:8080/form
	r.POST("/form", func(c *gin.Context) {
			var form StealerForm
			c.BindWith(&form, binding.Form)

			//execute script
			stopped_prod_user_id, stopped_auction_counter, page_when_script_stopped_working := stealerPkg.Ebay_prod_to_sand_stealer(form.Add_item_verb, form.Items_per_page, form.Page_number, form.Sort, form.Prod_user_id, form.Prod_ebay_token, form.Sand_ebay_token, c, form.Site_id, form.Paypal)
			//write something
			c.String(200, "Script finished working for user "+stopped_prod_user_id+" @page "+strconv.Itoa(page_when_script_stopped_working)+"; auctions scanned "+strconv.Itoa(stopped_auction_counter)+".\n ")
			fmt.Println(200, "Script finished working for user "+stopped_prod_user_id+" @page "+strconv.Itoa(page_when_script_stopped_working)+"; auctions scanned "+strconv.Itoa(stopped_auction_counter)+".\n ")
		})

	// Listen and server on 0.0.0.0:8080
	r.Run(":8080")
}
