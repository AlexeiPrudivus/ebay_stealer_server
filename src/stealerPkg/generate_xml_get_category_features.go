package stealerPkg

func Generate_xml_get_category_features(token, category_id string)(string){

	get_category_features_xml :=    "<?xml version='1.0' encoding='utf-8'?>"+
									"<GetCategoryFeaturesRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
										"<RequesterCredentials><eBayAuthToken>"+token+"</eBayAuthToken></RequesterCredentials>"+
										"<DetailLevel>ReturnAll</DetailLevel>"+
										"<ViewAllNodes>true</ViewAllNodes>"+
										"<CategoryID>"+category_id+"</CategoryID>"+
										"<FeatureID>ConditionEnabled</FeatureID>"+
										"<ErrorLanguage>en_US</ErrorLanguage>"+
										"<WarningLevel>High</WarningLevel>"+
									"</GetCategoryFeaturesRequest>​"

	return get_category_features_xml
}
