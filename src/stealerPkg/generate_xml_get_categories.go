package stealerPkg

func Generate_xml_get_categories(token, auction_id, site_id string)(string){

	get_categories_xml :=    "<?xml version='1.0' encoding='utf-8'?>"+
								"<GetCategoriesRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
									"<RequesterCredentials><eBayAuthToken>"+token+"</eBayAuthToken></RequesterCredentials>"+
									"<CategorySiteID>"+site_id+"</CategorySiteID>"+
									//"<CategoryParent>"+auction_id+"</CategoryParent>"+
									"<DetailLevel>ReturnAll</DetailLevel>"+
									"<LevelLimit>1</LevelLimit>"+
									"<OutputSelector>CategoryID</OutputSelector>"+
									//"<OutputSelector>CategoryLevel</OutputSelector>"+
									//"<OutputSelector>CategoryName</OutputSelector>"+
									"<OutputSelector>CategoryParentID</OutputSelector>"+
									//"<OutputSelector>LeafCategory</OutputSelector>"+
								"</GetCategoriesRequest>"

	return get_categories_xml
}
