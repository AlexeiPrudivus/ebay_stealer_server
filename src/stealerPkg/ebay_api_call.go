package stealerPkg
import (
	"github.com/PuerkitoBio/goquery"
	"net/http"
	"strings"
	"fmt"
	"regexp"
//	"strconv"
	"strconv"
)

func Ebay_api_call(verb, request_xml_body, ebay_type, site_id string)(map[string]string, []string, []string){
//=========================================== Generate and execute XmlHttpRequest ===========================================//
	client := &http.Client{}

	//will be used to return result
	var ebay_api_call_return map[string]string
	ebay_api_call_return = make(map[string]string)
	picture_urls := make([]string, 12)

	server_url := ""

	if ebay_type == "prod" {
		server_url = Server_url_prod
	} else if ebay_type == "sand" {
		server_url = Server_url_sand
	}

	//prudivusPkg.Server_url+strconv.Itoa(item_id) //other url. to recieve html of the item page
	request, err := http.NewRequest("POST", server_url, strings.NewReader(request_xml_body))
	if err != nil {
		fmt.Println("HttpRequest failed %s", err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error generating request"
		ebay_api_call_return["error"] = "-2"
		return ebay_api_call_return, []string{""}, []string{""}
	}

	//add headers to request
	request.Header.Add("X-EBAY-API-COMPATIBILITY-LEVEL", Compatibility_level)
	if ebay_type == "prod" {
		request.Header.Add("X-EBAY-API-DEV-NAME", Dev_id_prod)
		request.Header.Add("X-EBAY-API-APP-NAME", App_id_prod)
		request.Header.Add("X-EBAY-API-CERT-NAME", Cert_id_prod)
		request.Header.Add("X-EBAY-API-SITEID", site_id)
	} else if ebay_type == "sand" {
		request.Header.Add("X-EBAY-API-DEV-NAME", Dev_id_sand)
		request.Header.Add("X-EBAY-API-APP-NAME", App_id_sand)
		request.Header.Add("X-EBAY-API-CERT-NAME", Cert_id_sand)
		request.Header.Add("X-EBAY-API-SITEID", "0")
	}
	request.Header.Add("X-EBAY-API-CALL-NAME", verb)

	//execute request and get response
	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("%s", err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error sending request"
		ebay_api_call_return["error_id"] = "-1"
		return ebay_api_call_return, []string{""}, []string{""}
	}
	defer response.Body.Close()

	//html code of GetItem result
	responseDoc, err := goquery.NewDocumentFromResponse(response)
	if err != nil {
		fmt.Println("%s",err) //delete later
		ebay_api_call_return["status"] = "0"
		ebay_api_call_return["error"] = "Error using goquery"
		ebay_api_call_return["error"] = "-3"
		return ebay_api_call_return, []string{""}, []string{""}
	}
//============================================== if XML file has errors/warnings ============================================//
	errors :=responseDoc.Find("Errors") //fmt.Println(errors.Html())
	errors_html, _ := errors.Html()
	if errors_html != "" {
		errors_html = strings.Replace(errors_html, "&#34;", "\"", -1) //"
		errors_html = strings.Replace(errors_html, "&lt;", "<", -1) // <
		errors_html = strings.Replace(errors_html, "&gt;", ">", -1) // >
		errors_html = strings.Replace(errors_html, "&#39;", "'", -1) // '
		errors_html = strings.Replace(errors_html, "&amp;", "&", -1) // &
		//fmt.Println(request_xml_body) //delete later
		//fmt.Println(errors_html) //delete later

		//ebay_api_call_return must have errors or warnings if they will occur
		//set defaults to resultArray
		ebay_api_call_return["status"] = "1"
		ebay_api_call_return["error"] = ""
		ebay_api_call_return["error_id"] = ""
		ebay_api_call_return["warning_status"] = "0"
		ebay_api_call_return["warning"] = ""
		ebay_api_call_return["warning_id"] = ""

		//foreach error in errors
		responseDoc.Find("Errors").Each(func(i int, error *goquery.Selection) {
			//fmt.Println("error number", i) //index //delete later
			if i == 0 {
				//variables used within foreach loop
				short_msg := "";
				long_msg := "";
				error_id := "";

				//delete later
				//error_text := error.Find("longmessage").Text()
				//fmt.Println(error_text)
				error_code := error.Find("SeverityCode").Text()
				//fmt.Println("severity code value:", error_code)

				if error_code == "Warning" {
//================================================ if XML file has warnings =================================================//
					res_warning := ""
					/*if ebay_api_call_return["warning_status"] == "1" {
					continue
					}*/
					ebay_api_call_return["warning_status"] = "0"

					error_id = error.Find("ErrorCode").Text()
					short_msg = error.Find("ShortMessage").Text()
					long_msg = error.Find("LongMessage").Text()

					//fmt.Println("error_id:", error_id) //delete later
					//fmt.Println("short_msg:", short_msg) //delete later
					//fmt.Println("long_msg:", long_msg) //delete later

					res_warning = error_id+":"+short_msg

					//if there is a long message (ie ErrorLevel=1), display it
					if len(long_msg) > 0 {
						res_warning = error_id+":"+long_msg
					}

					ebay_api_call_return["warning_id"] = error_id
					ebay_api_call_return["warning"] = res_warning
				} else if error_code == "Error" {
//================================================== if XML file has errors =================================================//
					res_warning := ""
					/*if ebay_api_call_return["status"] == "0" {
					continue
					}*/
					ebay_api_call_return["status"] = "0"

					error_id = error.Find("ErrorCode").Text()
					short_msg = error.Find("ShortMessage").Text()
					long_msg = error.Find("LongMessage").Text()

					//fmt.Println("error_id:", error_id) //delete later
					//fmt.Println("short_msg:", short_msg) //delete later
					//fmt.Println("long_msg:", long_msg) //delete later

					res_warning = error_id+":"+short_msg

					//if there is a long message (ie ErrorLevel=1), display it
					if len(long_msg) > 0 {
						res_warning = error_id+":"+long_msg
					}

					ebay_api_call_return["error_id"] = error_id
					ebay_api_call_return["error"] = res_warning

					//TEMP
					if error_id == "10019" {
						fmt.Println("ErrorParameters:",error.Find("ErrorParameters").Text())
					}
				}
			}
		})
		return ebay_api_call_return, []string{""}, []string{""}
	} else {
//================================================= if XML file has no errors ===============================================//
		//get results nodes
		//responses := responseDoc.Find(verb+"Response");
		//responses_html, _ := responses.Html()
		if verb == "VerifyAddItem" || verb == "AddItem" {
//================================================== VerifyAddItem or AddItem ===============================================//
		//do nothing. only check errors
		} else if verb == "GetItem" {
//========================================================== GetItem ========================================================//
			//get as much info as possible
			//ebay_api_call_return["item_id"], _ = responseDoc.Find("ItemID").Html()
			ebay_api_call_return["title"], _ = responseDoc.Find("Title").Html()
			ebay_api_call_return["sub_title"], _ = responseDoc.Find("SubTitle").Html()
			ebay_api_call_return["description"], _ = responseDoc.Find("Description").Html()
			ebay_api_call_return["category_id"], _ = responseDoc.Find("CategoryID").Html()
			ebay_api_call_return["condition_id"], _ = responseDoc.Find("ConditionID").Html()
			if ebay_api_call_return["condition_id"] == "" {
				ebay_api_call_return["condition_id"] = "1000" //hardcoded because of category switch that happens during GetSuggestedCategory call
			}
			ebay_api_call_return["condition_description"], _ = responseDoc.Find("ConditionDisplayName").Html()
			ebay_api_call_return["start_price"], _ = responseDoc.Find("StartPrice").Html()
			ebay_api_call_return["buy_it_now_price"], _ = responseDoc.Find("BuyItNowPrice").Html()
			ebay_api_call_return["country"], _ = responseDoc.Find("Country").Html()
			//ebay_api_call_return["country"] = "US"
			ebay_api_call_return["currency"], _ = responseDoc.Find("Currency").Html()
			ebay_api_call_return["currency_id"], _ = responseDoc.Find("Currency").Attr("currencyid")
			//convert prices
			listing_details := responseDoc.Find("ListingDetails")
			ebay_api_call_return["converted_buy_it_now_price"], _ = listing_details.Find("ConvertedBuyItNowPrice").Html()
			ebay_api_call_return["converted_start_price"], _ = listing_details.Find("ConvertedStartPrice").Html()
			ebay_api_call_return["conv_currency_id"], _ = listing_details.Find("ConvertedStartPrice").Attr("currencyid")
			//fmt.Println("buy_it_now:",ebay_api_call_return["buy_it_now_price"], "currency:",ebay_api_call_return["currency"]) //delete later
			//fmt.Println("converted_but_it_now:",ebay_api_call_return["converted_buy_it_now_price"]) //delete later
			//fmt.Println("start_price:",ebay_api_call_return["start_price"]) //delete later
			//fmt.Println("converted_start:",ebay_api_call_return["converted_start_price"]) //delete later
			//fmt.Println("orig_currency:",ebay_api_call_return["currency"], "conv_currency:",ebay_api_call_return["conv_currency_id"]) //delete later

			ebay_api_call_return["dispatch_time_max"], _ = responseDoc.Find("DispatchTimeMax").Html()

			ebay_api_call_return["listing_duration"], _ = responseDoc.Find("ListingDuration").Html()
			//not helping?
			/*if ebay_api_call_return["listing_duration"] == "1" {
				ebay_api_call_return["listing_duration"] = "3"
			}*/

			ebay_api_call_return["listing_type"], _ = responseDoc.Find("ListingType").Html()
			ebay_api_call_return["location"], _ = responseDoc.Find("Location").Html()
			//ebay_api_call_return["location"] = "San Jose, CA"
			//fmt.Println("location:",ebay_api_call_return["location"]) //delete later
			//fmt.Println("country:",ebay_api_call_return["country"]) //delete later
			//item specifics fix
			ebay_api_call_return["item_specifics"], _ = responseDoc.Find("ItemSpecifics").Html()
				//find all broken tags and fix them
				reg := regexp.MustCompile( "<source/>(.*?)<" )
				source_array := reg.FindAllStringSubmatch(ebay_api_call_return["item_specifics"], -1)
				//fmt.Println(source_array) //delete later
				source_tag_names := make([]string, len(source_array))
				for i:=0;i<len(source_tag_names);i++ {
					source_tag_names[i] = source_array[i][1]
				}
				//fmt.Println(source_tag_names) //delete later
				source_tag_names = Unique_array(source_tag_names)
				//fmt.Println(source_tag_names) //delete later
				for _, source_tag := range source_tag_names {
					reg = regexp.MustCompile("<source/>"+source_tag)
					ebay_api_call_return["item_specifics"] =  reg.ReplaceAllString(ebay_api_call_return["item_specifics"], "<source>"+source_tag+"</source>")
				}

				//reg = regexp.MustCompile( "namevaluelist>" )
				//ebay_api_call_return["item_specifics"] =  reg.ReplaceAllString(ebay_api_call_return["item_specifics"], "NameValueList>")
				//ebay_api_call_return["item_specifics"] =  reg.ReplaceAllString(ebay_api_call_return["item_specifics"], "NameValueList>")
				//ebay_api_call_return["item_specifics"] =  reg.ReplaceAllString(ebay_api_call_return["item_specifics"], "NameValueList>")

				ebay_api_call_return["item_specifics"] = strings.Replace(ebay_api_call_return["item_specifics"], "namevaluelist", "NameValueList", -1)
				ebay_api_call_return["item_specifics"] = strings.Replace(ebay_api_call_return["item_specifics"], "name", "Name", -1)
				ebay_api_call_return["item_specifics"] = strings.Replace(ebay_api_call_return["item_specifics"], "value", "Value", -1)
				ebay_api_call_return["item_specifics"] = strings.Replace(ebay_api_call_return["item_specifics"], "source", "Source", -1)
				//fmt.Println("item_specifics:",ebay_api_call_return["item_specifics"]) //delete later

			//continue
			ebay_api_call_return["payment_methods"], _ = responseDoc.Find("PaymentMethods").Html()
			//ebay_api_call_return["paypal_email_address"], _ = responseDoc.Find("PayPalEmailAddress").Html()
			//gallery
			picture_details := responseDoc.Find("PictureDetails")
			ebay_api_call_return["picture_details"], _ = picture_details.Html()
			ebay_api_call_return["gallery_type"], _ = picture_details.Find("GalleryType").Html()
			ebay_api_call_return["gallery_url"], _ = picture_details.Find("GalleryURL").Html()
			ebay_api_call_return["photo_display"], _ = picture_details.Find("PhotoDisplay").Html()
			//ebay_api_call_return["picture_url"], _ = picture_details.Find("PictureURL").Html()
			//pictureURLs
			picture_details.Find("PictureURL").Each(func(i int, s *goquery.Selection) {
				picture_urls[i] = s.Text()
				//ebay_api_call_return["picture_url_"+strconv.Itoa(i)] := s.Text()
				//fmt.Println("picture_url_"+strconv.Itoa(i)," ",s.Text())
			})
			//fmt.Println(picture_urls) //delete later
			ebay_api_call_return["postal_code"], _ = responseDoc.Find("PostalCode").Html()

			ebay_api_call_return["quantity"], _ = responseDoc.Find("Quantity").Html()
			if ebay_api_call_return["quantity"] == "0" {
				ebay_api_call_return["quantity"] = "1"
			}
			//return policy
			return_policy := responseDoc.Find("ReturnPolicy")
			ebay_api_call_return["return_policy"], _ = return_policy.Html()
			//reg = regexp.MustCompile(" <nil>")
			//ebay_api_call_return["return_policy"] =  reg.ReplaceAllString(ebay_api_call_return["return_policy"], "")
			//fmt.Println(ebay_api_call_return["return_policy"]) //delete later

			ebay_api_call_return["returns_accepted_option"], _ = return_policy.Find("ReturnsAcceptedOption").Html()
			//not needed
			ebay_api_call_return["refund_option"], _ = return_policy.Find("RefundOption").Html()
			ebay_api_call_return["refund"], _ = return_policy.Find("Refund").Html()
			ebay_api_call_return["returns_within_option"], _ = return_policy.Find("ReturnsWithinOption").Html()
			ebay_api_call_return["returns_within"], _ = return_policy.Find("ReturnsWithin").Html()
			ebay_api_call_return["returns_accepted_option"], _ = return_policy.Find("ReturnsAcceptedOption").Html()
			ebay_api_call_return["returns_accepted"], _ = return_policy.Find("ReturnsAccepted").Html()
			ebay_api_call_return["returns_description"], _ = return_policy.Find("Description").Html()
			ebay_api_call_return["shipping_cost_paid_by_option"], _ = return_policy.Find("ShippingCostPaidByOption").Html()
			ebay_api_call_return["shipping_cost_paid_by"], _ = return_policy.Find("ShippingCostPaidBy").Html()

			//shipping details
			shipping_details := responseDoc.Find("ShippingDetails")
			//fmt.Println(shipping_details.Html()) //delete later
			ebay_api_call_return["shipping_details"], _ = shipping_details.Html()
			ebay_api_call_return["shipping_type"], _ = shipping_details.Find("ShippingType").Html()
			if ebay_api_call_return["shipping_type"] == "" {
				ebay_api_call_return["shipping_type"] = "NotSpecified"
			}
			//fmt.Println(ebay_api_call_return["shipping_type"]) //delete later
			//exclude ship to location
				reg = regexp.MustCompile("<excludeshiptolocation>.*</excludeshiptolocation>")
				exclude_location_array := reg.FindAllStringSubmatch(ebay_api_call_return["shipping_details"], -1)
			if len(exclude_location_array) > 0 {
				ebay_api_call_return["exclude_ship_to_location"] = exclude_location_array[0][0]
				ebay_api_call_return["exclude_ship_to_location"] = strings.Replace(ebay_api_call_return["exclude_ship_to_location"], "excludeshiptolocation", "ExcludeShipToLocation", -1)
			}
			//fmt.Println(ebay_api_call_return["exclude_ship_to_location"]) //delete later

			//domestic shipping
			shipping_service_options := shipping_details.Find("ShippingServiceOptions")
			//fmt.Println(shipping_service_options.Html()) //delete later
			ebay_api_call_return["shipping_service_priority"], _ = shipping_service_options.Find("ShippingServicePriority").Html()
			ebay_api_call_return["shipping_service"], _ = shipping_service_options.Find("ShippingService").Html()
			if ebay_api_call_return["shipping_service"] != "" { //site_id != "0"
				ebay_api_call_return["shipping_service"] = "USPSPriority" //standard shipping service for all copied auctions
			}
			ebay_api_call_return["shipping_service_cost"], _ = shipping_service_options.Find("ShippingServiceCost").Html()

			//international shipping
			int_shipping_service_option := shipping_details.Find("InternationalShippingServiceOption")
			//fmt.Println(int_shipping_service_option.Html()) //delete later
			ebay_api_call_return["int_shipping_service_option"], _ = shipping_details.Find("InternationalShippingServiceOption").Html()
			//fmt.Println(ebay_api_call_return["int_shipping_service_option"]) //delete later
			ebay_api_call_return["int_shipping_service_priority"], _ = int_shipping_service_option.Find("ShippingServicePriority").Html()
			ebay_api_call_return["int_shipping_service"], _ = int_shipping_service_option.Find("ShippingService").Html()
			if ebay_api_call_return["int_shipping_service"] != "" {
				ebay_api_call_return["int_shipping_service"] = "USPSFirstClassMailInternational" //standard int shipping service for all copied auctions
			}
			ebay_api_call_return["int_shipping_service_cost"], _ = int_shipping_service_option.Find("ShippingServiceCost").Html()
			if ebay_api_call_return["int_shipping_service_cost"] == "" {
				ebay_api_call_return["int_shipping_service_cost"] = "0.00"
			}
			ebay_api_call_return["int_ship_to_location"], _ = int_shipping_service_option.Find("ShipToLocation").Html()
			//ship to location
			if ebay_api_call_return["int_shipping_service_option"] != "" {
				reg = regexp.MustCompile("<shiptolocation>.*</shiptolocation>")
				location_array := reg.FindAllStringSubmatch(ebay_api_call_return["int_shipping_service_option"], -1)
				if len(exclude_location_array) > 0 {
					ebay_api_call_return["ship_to_location"] = location_array[0][0]
					ebay_api_call_return["ship_to_location"] = strings.Replace(ebay_api_call_return["ship_to_location"], "shiptolocation", "ShipToLocation", -1)
				} else {
					ebay_api_call_return["ship_to_location"] = "<ShipToLocation>Worldwide</ShipToLocation>"
				}
			}
			//fmt.Println(ebay_api_call_return["ship_to_location"]) //delete later
			//fmt.Println(ebay_api_call_return["ship_to_location"]) //delete later
			//fmt.Println("int option:",ebay_api_call_return["int_shipping_service_option"])
			//fmt.Println("int priority:",ebay_api_call_return["int_shipping_service_priority"])
			//fmt.Println("int service:",ebay_api_call_return["int_shipping_service"])
			//fmt.Println("int cost:",ebay_api_call_return["int_shipping_service_cost"])
			//fmt.Println("int ship to:",ebay_api_call_return["int_ship_to_location"])

			//shipping package details
			shipping_package_details := responseDoc.Find("ShippingPackageDetails")
			ebay_api_call_return["weight_major"], _ = shipping_package_details.Find("WeightMajor").Html()
			ebay_api_call_return["weight_minor"], _ = shipping_package_details.Find("WeightMinor").Html()
			ebay_api_call_return["measurement_system_major"], _ = shipping_package_details.Find("WeightMajor").Attr("measurementsystem")
			ebay_api_call_return["measurement_system_minor"], _ = shipping_package_details.Find("WeightMinor").Attr("measurementsystem")
			ebay_api_call_return["unit_major"], _ = shipping_package_details.Find("WeightMajor").Attr("unit")
			ebay_api_call_return["unit_minor"], _ = shipping_package_details.Find("WeightMinor").Attr("unit")
			//Enter a value between 0 and 15 for <WeightMinor> error. even when Metric system. hardcoded.
			if ebay_api_call_return["weight_minor"] != "" {
				weight_minor_check, _ := strconv.Atoi(ebay_api_call_return["weight_minor"])
				if weight_minor_check > 15 {
					ebay_api_call_return["weight_minor"] = "10"
				}
			}
			//fmt.Println("weight_major:",ebay_api_call_return["weight_major"], "system_major:",ebay_api_call_return["measurement_system_major"], "unit_major:",ebay_api_call_return["unit_major"]) //delete later
			//fmt.Println("weight_minor:",ebay_api_call_return["weight_minor"], "system_minor:",ebay_api_call_return["measurement_system_minor"], "unit_minor:",ebay_api_call_return["unit_minor"]) //delete later

			ebay_api_call_return["site"], _ = responseDoc.Find("Site").Html()
			ebay_api_call_return["variations"], _ = responseDoc.Find("Variations").Html()
			//fmt.Println(ebay_api_call_return["variations"]) //delete later

			if ebay_api_call_return["variations"] != "" {
				reg = regexp.MustCompile("<variationspecificsset>.*</variationspecificsset>")
				variations_node := reg.FindAllStringSubmatch(ebay_api_call_return["variations"], -1)
				//fmt.Println(variations_node) //delete later
				ebay_api_call_return["variations"] = reg.ReplaceAllString(ebay_api_call_return["variations"], "")
				//reg = regexp.MustCompile("<pictures>.*</pictures>")
				//ebay_api_call_return["variations"] = reg.ReplaceAllString(ebay_api_call_return["variations"], "")
				//fmt.Println(ebay_api_call_return["variations"]) //delete later
				ebay_api_call_return["variations"] = variations_node[0][0]+ebay_api_call_return["variations"]
				//fmt.Println(ebay_api_call_return["variations"]) //delete later
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "variationspecificname", "VariationSpecificName", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "variationspecificpictureset", "VariationSpecificPictureSet", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "variationspecificvalue", "VariationSpecificValue", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "pictureurl", "PictureURL", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "pictures", "Pictures", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "variationspecificsset", "VariationSpecificsSet", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "namevaluelist", "NameValueList", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "sku", "SKU", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "variationspecifics", "VariationSpecifics", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "quantitysoldbupickupinstore", "QuantitySoldByPickupInStore", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "quantitysold", "QuantitySold", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "sellingstatus", "SellingStatus", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "startprice", "StartPrice", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "variation", "Variation", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "value", "Value", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "quantity", "Quantity", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "name", "Name", -1)
				ebay_api_call_return["variations"] = strings.Replace(ebay_api_call_return["variations"], "currencyid", "currencyID", -1)
				//fmt.Println(ebay_api_call_return["variations"]) //delete later
				//ebay_api_call_return["uuid"], _ = responseDoc.Find("UUID").Html()
				//fmt.Println(ebay_api_call_return["uuid"]) //delete later
			}
		} else if verb == "GetSellerList" {
//======================================================= GetSellerList =====================================================//
			has_more_items, _ := responseDoc.Find("HasMoreItems").Html()
			ebay_api_call_return["has_more_items"] = has_more_items

			item_array, _ := responseDoc.Find("itemarray").Html()
			//ebay_api_call_return["item_array"] = item_array //delete later

			//find all auction_ids here
			reg, err := regexp.Compile("<item><itemid>([^\"]*?)</itemid></item>")
			if err != nil {
				fmt.Println(err)
			}
			reg_result_array := reg.FindAllStringSubmatch(item_array, -1)
			//fmt.Printf("%#v\n", reg_result_array) //delete later

			auction_ids := make([]string, len(reg_result_array))
			u := 0
			for i, reg_1 := range reg_result_array {
				for j, reg_2 := range reg_1 {
					if j == 1 {
						auction_ids[i] = reg_2
						u++
					}
				}
			}
			//fmt.Println("auction_ids:",auction_ids) //delete later
			ebay_api_call_return["status"] = "1"
			return ebay_api_call_return, auction_ids, []string{""}
		} else if verb == "GetSuggestedCategories" {
			ebay_api_call_return["category_id"],_ = responseDoc.Find("CategoryID").Html()
			ebay_api_call_return["category_parent_id"],_ = responseDoc.Find("CategoryParentID").Html()
			//ebay_api_call_return["suggested_categories"],_ = responseDoc.Find("SuggestedCategoryArray").Html()
		} else if verb == "GetCategoryFeatures" {
			ebay_api_call_return["category_id_2"],_ = responseDoc.Find("CategoryID").Html()
			ebay_api_call_return["condition_enabled"],_ = responseDoc.Find("ConditionEnabled").Html()
		}

		//fmt.Println(responses.Html())
		//fmt.Println(responses)
		//responses := responseDoc.Find(verb+"Response");
		ebay_api_call_return["status"] = "1"
		//ebay_api_call_return["items"] = responses_html
		return ebay_api_call_return, []string{""}, picture_urls
	}
//description :=responseDoc.Find("Description")
//fmt.Println(description.Html())
}
