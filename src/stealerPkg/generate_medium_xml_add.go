package stealerPkg

func Generate_medium_xml_add(token string, ebay_item_body_response map[string]string, add_item_verb, auction_id string, picture_urls []string, paypal string)(string){

	create_item_xml := "<?xml version='1.0' encoding='utf-8'?>"+
						"<"+add_item_verb+"Request xmlns='urn:ebay:apis:eBLBaseComponents'>"+
							"<RequesterCredentials><eBayAuthToken>"+token+"</eBayAuthToken></RequesterCredentials>"+
							"<ErrorLanguage>en_US</ErrorLanguage>"+
							"<WarningLevel>High</WarningLevel>"+
							"<Item>"+
								"<Title>"+ebay_item_body_response["title"]+"</Title>"+ //german äöüßé //french èçëòôöùàâ//spanish ñ
								"<Description>"+ebay_item_body_response["description"]+"</Description>"+
								"<PrimaryCategory><CategoryID>"+ebay_item_body_response["category_id"]+"</CategoryID></PrimaryCategory>"+
								"<PrivateListing>False</PrivateListing>"+
								"<StartPrice currencyID='USD'>"+ebay_item_body_response["converted_start_price"]+"</StartPrice>"
	if ebay_item_body_response["converted_buy_it_now_price"] != "" {
			create_item_xml +=	"<BuyItNowPrice currencyID='USD'>"+ebay_item_body_response["converted_buy_it_now_price"]+"</BuyItNowPrice>" //converted_ //currencyID='USD'
	}
			create_item_xml +=	"<ConditionID>1000</ConditionID>"+
								"<CategoryMappingAllowed>true</CategoryMappingAllowed>"+
								"<Country>US</Country>"+
								"<Currency>USD</Currency>"+
								"<DispatchTimeMax>3</DispatchTimeMax>"+
								"<ListingDuration>"+ebay_item_body_response["listing_duration"]+"</ListingDuration>"+
								"<ListingType>"+ebay_item_body_response["listing_type"]+"</ListingType>"+ //FixedPriceItem
								"<Location>US</Location>"+
								"<ItemSpecifics>"+ebay_item_body_response["item_specifics"]+"</ItemSpecifics>"+
								"<PaymentMethods>PayPal</PaymentMethods>"+
								"<PayPalEmailAddress>aprudivus@gmail.com</PayPalEmailAddress>"+
								"<PictureDetails>"+
									"<GalleryType>"+ebay_item_body_response["gallery_type"]+"</GalleryType>"+
									"<GalleryURL>"+ebay_item_body_response["gallery_url"]+"</GalleryURL>"+
									"<PhotoDisplay>"+ebay_item_body_response["photo_display"]+"</PhotoDisplay>"
									for _,picture_url := range picture_urls {
										if picture_url == "" {
											break
										} else {
				create_item_xml += "<PictureURL>"+picture_url+"</PictureURL>"
										}
									}
			create_item_xml +=  "</PictureDetails>"+
								"<Quantity>1</Quantity>"+
								"<ReturnPolicy><ReturnsAcceptedOption>ReturnsNotAccepted</ReturnsAcceptedOption></ReturnPolicy>"+
								"<ShippingDetails>"+
									"<ShippingType>Flat</ShippingType>"+
									"<ShippingServiceOptions>"+
										"<ShippingServicePriority>1</ShippingServicePriority>"+
										"<ShippingService>Pickup</ShippingService>"+
										"<ShippingServiceCost currencyID='USD'>0.0</ShippingServiceCost>"+
									"</ShippingServiceOptions>"+
								"</ShippingDetails>"+
								"<Site>US</Site>"+
								"<UUID>aaaaa00000bbbbb11111"+auction_id+"</UUID>"+
							"</Item>"+
						"</"+add_item_verb+"Request>"

	return create_item_xml
}
