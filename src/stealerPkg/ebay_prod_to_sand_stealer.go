package stealerPkg

import (
	"github.com/gin-gonic/gin"
	"fmt"
//	"strconv"
	"time"
	"strings"
//	"regexp"
)

func Ebay_prod_to_sand_stealer(add_item_verb string, items_per_page, page_number, sort int, prod_user_id, prod_ebay_token, sand_ebay_token string, c *gin.Context, site_id, paypal string)(stopped_prod_user_id string, stopped_auction_counter, stopped_at_page_number int) {
	//output all received arguments
	fmt.Println("add_item_verb",add_item_verb)
	fmt.Println("items_per_page",items_per_page)
	fmt.Println("page_number",page_number)
	fmt.Println("sort",sort)
	fmt.Println("prod_user_id",prod_user_id)
	fmt.Println("site_id",site_id)
	fmt.Println("paypal",paypal)
	fmt.Println("prod_ebay_token",prod_ebay_token)
	fmt.Println("sand_ebay_token",sand_ebay_token)

	t := time.Now().Local()
	end_time_from := string(t.Format("2006-01-02T15:04:05.768Z"))
	end_time_to := string(t.AddDate(0, 0, 90).Format("2006-01-02T15:04:05.768Z"))
	fmt.Println("end_time_from",end_time_from)
	fmt.Println("end_time_to",end_time_to)

	fmt.Println("script started working. please wait.")
	//set up variables(later they will be received from a html form somehow)
	//add_item_verb := "VerifyAddItem" //+
	//end_time_from := "2014-12-20T19:09:02.768Z" //auctions ending before a date. lower value in range //should be changeable in form-
	//end_time_to := "2015-02-04T19:09:02.768Z" //auctions ending before a date. higher value in range //should be changeable in form-
	variations := 0 //1=show variations. 0=don't show
	//items_per_page := 200 //should be changeable in form //the closer the number is to 200-> the better+
	//page_number := 1 //default value+
	//sort := 2 //0 = No sorting //1 = Sort in descending order //2 = Sort in ascending order //should be changeable in form
	//prod_user_id := "eforcity" //should be changeable in form+
	//token := User_token_prod //should be changeable in form-
	continue_variable := "true" //default value. this variable stops the loop is ebay api says that there are no more items to process
	//stopped_auction_id := "" //indicates auction id that was being worked with when the script stopped working
	auction_counter := 0

//======================================= everything is happening within a while loop =======================================//
	for continue_variable == "true" {
//========================================== get an array of auction_ids of a user ==========================================//
		//generate Xml configured to get auction ids from a certain user
		get_item_ids_xml := Generate_xml_ids(end_time_from, end_time_to, variations, items_per_page, page_number, sort, prod_user_id, prod_ebay_token)

		//execute Ebay_api_call function
		ebay_item_body_response, auction_ids, _ := Ebay_api_call("GetSellerList", get_item_ids_xml, "prod", site_id)

		//if there was an error in executing request
		if ebay_item_body_response["status"] == "0" {
			if ebay_item_body_response["error"] != "" {
				fmt.Println("GetSellerList Error_body:", ebay_item_body_response["error"], "\n")
			} else if ebay_item_body_response["warning"] != "" {
				fmt.Println("GetSellerList Warning_body:", ebay_item_body_response["warning"], "\n")
			}
			//will be break here most likely
		} else {
			fmt.Println("Auction ids:", auction_ids)
			fmt.Println("Page", page_number, "will be processed. it is not the last page:", ebay_item_body_response["has_more_items"])
			fmt.Println("Request to get auction ids from user", prod_user_id, "executed with status:", ebay_item_body_response["status"], "\n")
		}

//======================================== we have an array of auction_ids of a user ========================================//
//====================== for each auction_id we copy it's description to the specified sandbox account ======================//
		for _, auction_id := range auction_ids {
			//generate Xml configured to get item description
			get_auction_description_xml := Generate_xml_get(auction_id, prod_ebay_token)

			//execute Ebay_api_call function
			ebay_item_body_response, _, picture_urls := Ebay_api_call("GetItem", get_auction_description_xml, "prod", site_id)
			fmt.Print("HttpRequest GetItem for item ", auction_id, " executed with status: ", ebay_item_body_response["status"],"; ")

			//if there was an error in executing request
			if ebay_item_body_response["status"] == "0" {
				if ebay_item_body_response["error"] != "" {
					fmt.Println("GetItem Error_body:", ebay_item_body_response["error"], "\n")
				} else if ebay_item_body_response["warning"] != "" {
					fmt.Println("GetItem Warning_body:", ebay_item_body_response["warning"], "\n")
				}
			} else {
				if ebay_item_body_response["description"] == "" {
					fmt.Println("Description for ", auction_id, " is empty\n")
				} else {
//================================== if we have an auction description and it is not empty ==================================//
					fmt.Print("Description for item ", auction_id, " retrieved with status: ", ebay_item_body_response["status"],"; ")
					//first we get the suggested category in the sandbox ebay for the current auction
//======================================== get suggested category for current auction =======================================//
					//generate xml GetSuggestedCategories xml file
					get_suggested_categories_xml := Generate_xml_get_suggested_categories(ebay_item_body_response["title"], prod_ebay_token)

					//execute Ebay_api_call function
					ebay_suggested_categories_response, _, _ := Ebay_api_call("GetSuggestedCategories", get_suggested_categories_xml, "prod", "0")

					//if there was an error in executing request
					if ebay_suggested_categories_response["status"] == "0" {
						if ebay_suggested_categories_response["error"] != "" {
							fmt.Println("GetSuggestedCategories Error_body:", ebay_suggested_categories_response["error"])
						} else if ebay_suggested_categories_response["warning"] != "" {
							fmt.Println("GetSuggestedCategories Warning_body:", ebay_suggested_categories_response["warning"])
						}
					} else {
						//if http request to get a suggested category id was successful, use it's result
						fmt.Println("Suggested category_id:", ebay_suggested_categories_response["category_id"]) //delete later
						if ebay_suggested_categories_response["category_id"] != "" {
							if ebay_suggested_categories_response["category_parent_id"] == "6000" {
								ebay_suggested_categories_response["category_id"] = "293" //no support for ebay motors
							}
							ebay_item_body_response["category_id"] = ebay_suggested_categories_response["category_id"]

//===================================== for suggested category check required features ======================================//
							//generate xml GetCategoryFeatures xml file
							get_category_features_xml := Generate_xml_get_category_features(prod_ebay_token, ebay_item_body_response["category_id"])

							//execute Ebay_api_call function
							ebay_category_features_response, _, _ := Ebay_api_call("GetCategoryFeatures", get_category_features_xml, "prod", "0")

							//if there was an error in executing request
							if ebay_category_features_response["status"] == "0" {
								if ebay_category_features_response["error"] != "" {
									fmt.Println("GetCategoryFeatures Error_body:", ebay_category_features_response["error"], "\n")
								} else if ebay_category_features_response["warning"] != "" {
									fmt.Println("GetCategoryFeatures Warning_body:", ebay_category_features_response["warning"], "\n")
								}
							} else {
								//if http request to get a suggested category id was successful, use it's result
								//fmt.Println("Category_ID:",ebay_category_features_response["category_id_2"], "Condition_enabled:",ebay_category_features_response["condition_enabled"]) //delete later
								ebay_item_body_response["condition_enabled"] = ebay_category_features_response["condition_enabled"]
							}
						}
					}
//==================================== we add a new auction to specified sandbox account ====================================//
					//change item verb to use item variations
					//generate Xml configured to add item to sandbox //VerifyAddItem //AddItem //VerifyFixedPriceAddItem //FixedPriceAddItem
					//create variables to switch between //VerifyAddItem //AddItem //VerifyFixedPriceAddItem //FixedPriceAddItem
					create_item_xml := ""
					add_item_verb_temp := "" //should be empty
					if ebay_item_body_response["variations"] == "" {
						add_item_verb_temp = strings.Replace(add_item_verb, "FixedPrice", "", -1)
					//MEDIUM Generate_medium_xml_add //can be basic Generate_basic_xml_add //can be normal Generate_xml_add
						create_item_xml = Generate_medium_xml_add(sand_ebay_token, ebay_item_body_response, add_item_verb_temp, auction_id, picture_urls, paypal)
					} else {
						add_item_verb_temp = add_item_verb
						create_item_xml = Generate_medium_xml_add_fixed(sand_ebay_token, ebay_item_body_response, add_item_verb_temp, auction_id, picture_urls, paypal)
					}

					//execute Ebay_api_call function //VerifyAddItem
					ebay_item_body_response, _, _ := Ebay_api_call(add_item_verb_temp, create_item_xml, "sand", site_id) //VerifyAddItem
					fmt.Print("HttpRequest "+add_item_verb_temp+" for item ", auction_id, " executed with status: ", ebay_item_body_response["status"],"; ")

					//if error in executing request
					if ebay_item_body_response["status"] == "0" {
						if ebay_item_body_response["error"] != "" {
							fmt.Println(add_item_verb_temp," Error_body:", ebay_item_body_response["error"], "\n")
						} else if ebay_item_body_response["warning"] != "" {
							fmt.Println(add_item_verb_temp, "Warning_body:", ebay_item_body_response["warning"], "\n")
						}
					} else {
						fmt.Println("Request to copy item", auction_id, "executed with status:", ebay_item_body_response["status"],"\n")
					}
				}
			}
			//time.Sleep(5001 * time.Millisecond) //if ebay allows us to use api only once in 5 sec //should be able to 100% work on any CPU
			//time.Sleep(3501 * time.Millisecond)
			//stopped_auction_id = auction_id
			auction_counter++
		}
		continue_variable = ebay_item_body_response["has_more_items"]
		page_number++
	}
	page_number--
	return prod_user_id, auction_counter, page_number
}
