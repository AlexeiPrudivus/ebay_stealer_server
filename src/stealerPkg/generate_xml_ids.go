package stealerPkg

import (
	"strconv"
)

func Generate_xml_ids(end_time_from, end_time_to string, variations, items_per_page, page_number, sort int, prod_user_id, token string)(string){
	get_item_ids_xml := "<?xml version='1.0' encoding='utf-8'?>"+
						"<GetSellerListRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
							"<EndTimeFrom>"+end_time_from+"</EndTimeFrom>"+
							"<EndTimeTo>"+end_time_to+"</EndTimeTo>"+
							"<IncludeVariations>"+strconv.Itoa(variations)+"</IncludeVariations>"+
							"<Pagination>"+
								"<EntriesPerPage>"+strconv.Itoa(items_per_page)+"</EntriesPerPage>"+
								"<PageNumber>"+strconv.Itoa(page_number)+"</PageNumber>"+
							"</Pagination>"+
							"<Sort>"+strconv.Itoa(sort)+"</Sort>"+
							"<UserID>"+prod_user_id+"</UserID>"+
							"<ErrorLanguage>en_US</ErrorLanguage>"+
							"<OutputSelector>HasMoreItems</OutputSelector>"+
							"<OutputSelector>itemId</OutputSelector>"+
							"<Version>"+Compatibility_level+"</Version>"+
							"<WarningLevel>High</WarningLevel>"+
							"<RequesterCredentials>"+
								"<eBayAuthToken>"+token+"</eBayAuthToken>"+
							"</RequesterCredentials>"+
						"</GetSellerListRequest>"

	return get_item_ids_xml
}
