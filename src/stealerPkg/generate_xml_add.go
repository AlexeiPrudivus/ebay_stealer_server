package stealerPkg

func Generate_xml_add(token string, ebay_item_body_response map[string]string, add_item_verb, auction_id string, picture_urls []string, paypal string)(string){

	//VerifyAddItem //AddItem
	create_item_xml := "<?xml version='1.0' encoding='utf-8'?>"+
						"<"+add_item_verb+"Request xmlns='urn:ebay:apis:eBLBaseComponents'>"+
							"<RequesterCredentials><eBayAuthToken>"+token+"</eBayAuthToken></RequesterCredentials>"+
							"<ErrorLanguage>en_US</ErrorLanguage>"+
							"<WarningLevel>High</WarningLevel>"+
							"<Item>"+
								"<Title>"+ebay_item_body_response["title"]+"</Title>"
	if ebay_item_body_response["sub_title"] != "" {
			create_item_xml +=	"<SubTitle>"+ebay_item_body_response["sub_title"]+"</SubTitle>"
	}
			create_item_xml +=	"<Description>"+ebay_item_body_response["description"]+"</Description>"+
								"<PrimaryCategory><CategoryID>"+ebay_item_body_response["category_id"]+"</CategoryID></PrimaryCategory>"+ //"+ebay_item_body_response["category_id"]+" 20349
								"<PrivateListing>False</PrivateListing>"
	if ebay_item_body_response["exclude_ship_to_location"] != "" {
			create_item_xml +=  "<BuyerRequirementDetails><ShipToRegistrationCountry>True</ShipToRegistrationCountry></BuyerRequirementDetails>"
	}
			create_item_xml += 	"<StartPrice currencyID='USD'>"+ebay_item_body_response["converted_start_price"]+"</StartPrice>" //+ //converted_ //currencyID='USD'
	if ebay_item_body_response["converted_buy_it_now_price"] != "" {
			create_item_xml +=	"<BuyItNowPrice currencyID='USD'>"+ebay_item_body_response["converted_buy_it_now_price"]+"</BuyItNowPrice>" //converted_ //currencyID='USD'
	}
	if ebay_item_body_response["condition_enabled"] == "Required" {
			create_item_xml +=  "<ConditionID>"+ebay_item_body_response["condition_id"]+"</ConditionID>"+
								"<ConditionDescription>"+ebay_item_body_response["condition_description"]+"</ConditionDescription>"
	}
			create_item_xml +=  "<CategoryMappingAllowed>true</CategoryMappingAllowed>"+
								"<Country>"+ebay_item_body_response["country"]+"</Country>"+
								"<Currency>USD</Currency>" //conv_currency_id //+ebay_item_body_response["currency"]+
	if ebay_item_body_response["dispatch_time_max"] != "" {
			create_item_xml +=  "<DispatchTimeMax>"+ebay_item_body_response["dispatch_time_max"]+"</DispatchTimeMax>"
	}
			create_item_xml +=	"<ListingDuration>"+ebay_item_body_response["listing_duration"]+"</ListingDuration>"+
								"<ListingType>"+ebay_item_body_response["listing_type"]+"</ListingType>"+
								"<Location>"+ebay_item_body_response["location"]+"</Location>"+
								"<ItemSpecifics>"+ebay_item_body_response["item_specifics"]+"</ItemSpecifics>"
			create_item_xml +=	"<PaymentMethods>"+ebay_item_body_response["payment_methods"]+"</PaymentMethods>"+
								"<PayPalEmailAddress>"+paypal+"</PayPalEmailAddress>"+
								"<PictureDetails>"+
									"<GalleryType>"+ebay_item_body_response["gallery_type"]+"</GalleryType>"+
									"<GalleryURL>"+ebay_item_body_response["gallery_url"]+"</GalleryURL>"+
									"<PhotoDisplay>"+ebay_item_body_response["photo_display"]+"</PhotoDisplay>"
									for _,picture_url := range picture_urls {
										if picture_url == "" {
											break
										} else {
				create_item_xml += "<PictureURL>"+picture_url+"</PictureURL>"
										}
									}
			create_item_xml +=  "</PictureDetails>"+
								"<PostalCode>"+ebay_item_body_response["postal_code"]+"</PostalCode>"+
								"<Quantity>"+ebay_item_body_response["quantity"]+"</Quantity>"+
								"<ReturnPolicy>"//+ebay_item_body_response["return_policy"]
									//"<ReturnsAcceptedOption>ReturnsNotAccepted</ReturnsAcceptedOption>"
	if ebay_item_body_response["returns_accepted_option"] != "" {
				create_item_xml +=  "<ReturnsAcceptedOption>"+ebay_item_body_response["returns_accepted_option"]+"</ReturnsAcceptedOption>"//+
									//"<ReturnsAccepted>"+ebay_item_body_response["returns_accepted"]+"</ReturnsAccepted>"
	}
	if ebay_item_body_response["refund_option"] != "" {
				create_item_xml +=  "<RefundOption>"+ebay_item_body_response["refund_option"]+"</RefundOption>"//+
									//"<Refund>"+ebay_item_body_response["refund"]+"</Refund>"
	}
	if ebay_item_body_response["returns_within_option"] != "" {
				create_item_xml +=  "<ReturnsWithinOption>"+ebay_item_body_response["returns_within_option"]+"</ReturnsWithinOption>"//+
									//"<ReturnsWithin>"+ebay_item_body_response["returns_within"]+"</ReturnsWithin>"
	}
	if ebay_item_body_response["returns_description"] != "" {
				create_item_xml +=  "<Description>"+ebay_item_body_response["returns_description"]+"</Description>"
	}
	if ebay_item_body_response["shipping_cost_paid_by_option"] != "" {
				create_item_xml +=  "<ShippingCostPaidByOption>"+ebay_item_body_response["shipping_cost_paid_by_option"]+"</ShippingCostPaidByOption>"//+
									//"<ShippingCostPaidBy>"+ebay_item_body_response["shipping_cost_paid_by"]+"</ShippingCostPaidBy>"
	}
			create_item_xml +=  "</ReturnPolicy>"+
								"<ShippingDetails>"+
									ebay_item_body_response["exclude_ship_to_location"]
									/*"<ShippingServiceOptions>"+
										"<ShippingServicePriority>1</ShippingServicePriority>"+
										"<ShippingService>Pickup</ShippingService>"+
										"<ShippingServiceCost currencyID='USD'>0.0</ShippingServiceCost>"+
									"</ShippingServiceOptions>"*/
	if ebay_item_body_response["shipping_type"] == "NotSpecified" {
				create_item_xml +=	"<ShippingType>"+ebay_item_body_response["shipping_type"]+"</ShippingType>"
	} else { //if ebay_item_body_response["shipping_type"] == "Flat" || ebay_item_body_response["shipping_type"] == "Calculated"
				create_item_xml +=	"<ShippingType>Flat</ShippingType>"+ //"+ebay_item_body_response["shipping_type"]+"
									"<ShippingServiceOptions>"+
										"<ShippingServicePriority>1</ShippingServicePriority>"+ //"+ebay_item_body_response["shipping_service_priority"]+"
										"<ShippingService>"+ebay_item_body_response["shipping_service"]+"</ShippingService>"
		//if ebay_item_body_response["shipping_service_cost"] != "" {
					create_item_xml +=  "<ShippingServiceCost currencyID='USD'>0.0</ShippingServiceCost>" //"+ebay_item_body_response["shipping_service_cost"]+"
		//}
				create_item_xml	+=  "</ShippingServiceOptions>"
		if ebay_item_body_response["int_shipping_service_option"] != "" {
				create_item_xml +=	"<InternationalShippingServiceOption>"+
										"<ShippingService>"+ebay_item_body_response["int_shipping_service"]+"</ShippingService>"
			//if ebay_item_body_response["int_shipping_service_priority"] != "" {
					create_item_xml +=	"<ShippingServicePriority>1</ShippingServicePriority>" //ebay_item_body_response["int_shipping_service_priority"]
			//}
			//if ebay_item_body_response["int_shipping_service_cost"] != "" && ebay_item_body_response["shipping_type"] != "Calculated" && ebay_item_body_response["shipping_type"] != "FlatDomesticCalculatedInternational" {
					create_item_xml +=  "<ShippingServiceCost currencyID='USD'>0.0</ShippingServiceCost>" //"+ebay_item_body_response["int_shipping_service_cost"]+"
			//}
					create_item_xml +=  ebay_item_body_response["ship_to_location"]+
									"</InternationalShippingServiceOption>"
		}
	}
			create_item_xml	+=  "</ShippingDetails>"
	if ebay_item_body_response["weight_major"] != "" && ebay_item_body_response["weight_minor"] != "" {
			create_item_xml	+=  "<ShippingPackageDetails>"+
				 					"<WeightMajor unit='"+ebay_item_body_response["unit_major"]+"' measurementSystem='"+ebay_item_body_response["measurement_system_major"]+"'>"+ebay_item_body_response["weight_major"]+"</WeightMajor>"+
									"<WeightMinor unit='"+ebay_item_body_response["unit_minor"]+"' measurementSystem='"+ebay_item_body_response["measurement_system_minor"]+"'>"+ebay_item_body_response["weight_minor"]+"</WeightMinor>"+
								"</ShippingPackageDetails>"
	}
			create_item_xml	+=  "<Site>"+ebay_item_body_response["site"]+"</Site>"+
								"<UUID>aaaaa00000bbbbb11111"+auction_id+"</UUID>"+ //aaaaa00000bbbbb11111"+auction_id+" //+ebay_item_body_response["uuid"]+
							"</Item>"+
						"</"+add_item_verb+"Request>" //VerifyAddItem //AddItem

	//fmt.Println(create_item_xml)
	return create_item_xml
}
