package stealerPkg

func Generate_xml_get_category_info(category_id string)(string){

	get_category_info_xml :=    "<?xml version='1.0' encoding='utf-8'?>"+
								"<GetCategoryInfoRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
									"<CategoryID>"+category_id+"</CategoryID>"+
									"<IncludeSelector>CategoryIDPath</IncludeSelector>"+
								"</GetCategoryInfoRequest>"

	return get_category_info_xml
}
