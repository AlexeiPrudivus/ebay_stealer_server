package stealerPkg

//===================================================== global variables ====================================================//
//ebay api user token for sandbox
//  const User_token_sand string = "AgAAAA**AQAAAA**aAAAAA**JvxqVA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhDJmLoQ2dj6x9nY+seQ**jB8DAA**AAMAAA**SV08+Y1N3fekdqHMwBas80sXRZ7RCcWnHTq8yBhJH16FhHPP6vBjIVQ3rSyE15u4/mH3etyK1PmHp1jUPxSFw+UrXr6jSrOYvr43VDf43IlMpU1/+3C5t6OsUn7bMgFVWeXzlNN69gks9QJ9VwQ5R9Gfxf87nUwdpG0EaY+Vgoow1DDnCLDZ5F8+Jn6Sm2jiOe3QAKNDq6phX515R7wF169uiKePvTZVF/lD9/tV/JbeJotP8bR8Zl8GOmlrTT5HZZz5pjA7VNsWA1p0KtiBOaTon1BTAYkuPv+JlqYgeEvXdf6eMif/+gipvEZe+BrYxTFHSpdTiSsAWjpoHiVGqJCyAnyzEkV0gE4kJR0rOM2v3/q5d4TThSwjycU1oq3EzcMFSNSFBgeofys18EP0h7va+gmijRCDt5DC2f4ZboJ6W/4i/TgQAiwfF2PlvVorJ/cXalMX2IdwOJX/Dze+aX5srpYmn39irw/W2WriXgSPMo0DM3gF5Mf+SUdBg81xAejGw9FHQSh5ANOT9SbU/zqLkaamtlIyS1MqqpQoXNODr9Y8Lzx2VivdGZI1D1XiVXSxtB/1W7xyWqjTwKeF5Mel+rA6X6ANc/ujx4/uKu7b+1TxJvIowJ3BHpLMZC+sue/tBKds8C23ZaV48NUiIRKGtKakyVWO+P7wHXpYmZsHd/rBDgl9PlXJ54RXpQNFot+6uT4MlcWNDPb7QvRJFcD7rOqLHl+9qMKcMqQg4zpOJWm5V5InRdXcgzi9d1kV"
//const User_token string = "AgAAAA**AQAAAA**aAAAAA**AWdHUA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wAkIeoDpGApQqdj6x9nY+seQ**2SkAAA**AAMAAA**NRBUit1IE+TRkdY1ENci+KHxryE18cQUA62ajB+W+T07vECn9tbsUC/gLejpzaEEsyjqkh0ve6dxq8x40Y77/W9QLkFh3lrg2oZbsPl35CH0gKLefQydofxS06jGqVzfYqU3sxM4jorJwo2TSKRGIk63ND/5e5pB6qHYCERXiQd1mVOibd5TI6OZBHWhIUm6VjuLJ0LInPj34oI7/04ZrvYxS2cHw0l9xZbEmqwFjP3yAAraJtQBGWxLpkSo/ebp/x4GPxVUe9wIEejhb/TUrtzJuaTFcijunmU//3LVzu6+3a3P9zNX5MgUxNHhac+X3naY3Tb+O7wgMBKech02yDk3DggsJgAiNt8EiTW0v5K363gmsOs6FE8dB/M/RlNwi7Prz0i2AS/LUfMrThoghnkdbUA3HULkNbTmXiGSGIP+Ahmr3PyDUy6AocWXVmkCF4JfJofN/HOLJ4v89DXEO5D5cukfCXZFarHdIws7z67wUnwd5CNYEFUM9TMy6E4S3bL0LiRRvb9+DiM1M6JdDEO387yow9i+f9hIyy8bSd0vyziKDGGkS6YlnoMaM1vxJqdHryW/Z+m3xT55f5wFwHTrmCSl7A6gHX6V9uaLX4BJtQRxhWw3uJkq9HXFum3RSgJjEvpl52+q4lq+d5s/b+22QV2TZZvCOhYYr9vxSTw/t7LPVmxtGUsC5x9I2ubeyYN1hxU4xDq9xAcCAA10ua5swDnMbZYegpdB+sCznyAOyn2pd8MTxtPK/hpFHYGr"
//  const User_token_prod string = "AgAAAA**AQAAAA**aAAAAA**026AVA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wGkIapCpGKoQqdj6x9nY+seQ**2SkAAA**AAMAAA**CAJeHfOD16lzDgUEsy2zxuxsm/iIPTvV1VdXF65pCJX5357fqADDzD9pp+9vWSZ6fY02l8HcqRJ5Z/PCBY4qHxDdYkOaxk5MEzP9Tlx0lazMiBwxyM4A+uT9TQX1i99KbSfW+SdWPn3/QnwaKTs3d4VyaHFXCMVyktuwZ4H1DH9u4ds3lDAphzRkPO7gD+XqT7p75cWhncUXUiYi/MfTKzNfXVqwwO567g+bKvCjSFxx4BpBxe/GM3Tm3uiKVfh4Q9hpbRL35ahByJPpwt5axlQ/zp0JAzWXVPYGPIPE0e5O0z8PtVa3CTRqk6rL33rmuDBOg7zEM/R1SqXDzNqN6U0dxOwGGO3cw/pyiq0Z8Vw3FSEIW6D28GoCjsYstAVu5/fxbFuUf6X9Dnsh3MX5SqxXcV/weUSHYIurt91FZpXd3Wm4BpH9wM1vftUK5M/IWWKgkblle5sAEzVLNyuGrQqLrKcIU1eCl07+SgQSF88mt3fK9VbI2RHV7BmQqFCi+I1ntwU80mIjPyvsPc8fupjoSX20rbq8UgkbgXWpBqF+9eYLPCN0fkOtDmMSOnwzOwysnJmMIJoH49x/Mpr/q09lVbWFaNRmqkrKoUInPQMUgvo9I0l+/guRYm+ifzyaCjcstxoWd1goLMPpw7VlecF58HG1ZjyMJbMmtYeBXVw5mrt1bbA6xGjJbCZDyDhc6dfY+RFpJ4bRAenj6poZWrKT/9FOhVGxDUK4/VztYQ7WnSdwoOGmOkdddcl/y3X4"

//version of api
const Compatibility_level string = "903"
//keys for ebay api sandbox
const Dev_id_sand string = "7204ff0d-a607-456e-829a-4255fe9afabe"
const App_id_sand string = "AlexeiPr-19be-4dfc-bb8f-7971e4ac458e"
const Cert_id_sand string = "355dc5ea-2a62-411c-a5d4-0f99a28e4aad"

const Dev_id_prod string = "K3S81234CQ3Z6L5H5AW2JG4MJBGP1D"
const App_id_prod string = "SELLERFOXIF15H7X2N3L8L12C941YH"
const Cert_id_prod string = "D97T1IF5K63$BP7286685-7ZUN45GA"
//ebay api url
const Server_url_prod string = "https://api.ebay.com/ws/api.dll"
const Server_url_sand string = "https://api.sandbox.ebay.com/ws/api.dll" //original/ will give xml
const Server_url_shop string = "http://open.api.ebay.com/shopping?"
//var Server_url string = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item=" //new/ not used/ will give html

//0 = usa
//var Site_id string = "0"
