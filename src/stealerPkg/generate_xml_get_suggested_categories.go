package stealerPkg

func Generate_xml_get_suggested_categories(title, token string)(string){

	get_suggested_categories_xml :=     "<?xml version='1.0' encoding='utf-8'?>"+
										"<GetSuggestedCategoriesRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
											"<Query>"+title+"</Query>"+
											"<RequesterCredentials><eBayAuthToken>"+token+"</eBayAuthToken></RequesterCredentials>"+
											"<ErrorLanguage>en_US</ErrorLanguage>"+
											"<WarningLevel>High</WarningLevel>"+
										"</GetSuggestedCategoriesRequest>"

	return get_suggested_categories_xml
}
