package stealerPkg

func Generate_medium_xml_add_fixed(token string, ebay_item_body_response map[string]string, add_item_verb, auction_id string, picture_urls []string, paypal string)(string){

	//fmt.Println("fixed:",add_item_verb)
	//fmt.Println("category_id:",ebay_item_body_response["category_id"])

	//VerifyAddFixedPriceItem //AddFixedPriceItem
	create_item_xml := "<?xml version='1.0' encoding='utf-8'?>"+
						"<"+add_item_verb+"Request xmlns='urn:ebay:apis:eBLBaseComponents'>"+
							"<RequesterCredentials><eBayAuthToken>"+token+"</eBayAuthToken></RequesterCredentials>"+
							"<ErrorLanguage>en_US</ErrorLanguage>"+
							"<WarningLevel>High</WarningLevel>"+
							"<Item>"+
								"<Title>"+ebay_item_body_response["title"]+"</Title>"+
								"<SubTitle>"+ebay_item_body_response["sub_title"]+"</SubTitle>"+
								"<Description>"+ebay_item_body_response["description"]+"</Description>"+
								"<PrimaryCategory><CategoryID>"+ebay_item_body_response["category_id"]+"</CategoryID></PrimaryCategory>"+ //"+ebay_item_body_response["category_id"]+" //20349
								"<PrivateListing>False</PrivateListing>"
	if ebay_item_body_response["condition_id"] != "" {
			create_item_xml +=  "<ConditionID>"+ebay_item_body_response["condition_id"]+"</ConditionID>"+
								"<ConditionDescription>"+ebay_item_body_response["condition_id"]+"</ConditionDescription>"
	}
			create_item_xml +=  "<CategoryMappingAllowed>true</CategoryMappingAllowed>"+
								"<Country>US</Country>"+
								"<Currency>USD</Currency>"+
								"<DispatchTimeMax>3</DispatchTimeMax>"+
								"<ListingDuration>"+ebay_item_body_response["listing_duration"]+"</ListingDuration>"+
								"<ListingType>"+ebay_item_body_response["listing_type"]+"</ListingType>"+
								"<Location>US</Location>"+
								"<ItemSpecifics>"+ebay_item_body_response["item_specifics"]+"</ItemSpecifics>"+
								"<PaymentMethods>PayPal</PaymentMethods>"+
								"<PayPalEmailAddress>"+paypal+"</PayPalEmailAddress>"+
								"<PictureDetails>"+
									"<GalleryType>"+ebay_item_body_response["gallery_type"]+"</GalleryType>"+
									"<GalleryURL>"+ebay_item_body_response["gallery_url"]+"</GalleryURL>"+
									"<PhotoDisplay>"+ebay_item_body_response["photo_display"]+"</PhotoDisplay>"
									for _,picture_url := range picture_urls {
										if picture_url == "" {
											break
										} else {
				create_item_xml += "<PictureURL>"+picture_url+"</PictureURL>"
										}
									}
			create_item_xml +=  "</PictureDetails>"+
								"<ReturnPolicy><ReturnsAcceptedOption>ReturnsNotAccepted</ReturnsAcceptedOption></ReturnPolicy>"+
								"<ShippingDetails>"+
									"<ShippingServiceOptions>"+
										"<ShippingServicePriority>1</ShippingServicePriority>"+
										"<ShippingService>Pickup</ShippingService>"+
										"<ShippingServiceCost currencyID='USD'>0.0</ShippingServiceCost>"+
									"</ShippingServiceOptions>"+
								"</ShippingDetails>"+
								"<Variations>"+ebay_item_body_response["variations"]+"</Variations>"+
								"<Site>"+ebay_item_body_response["site"]+"</Site>"+
								"<UUID>aaaaa00000bbbbb11111"+auction_id+"</UUID>"+ //aaaaa00000bbbbb11111"+auction_id+" //+ebay_item_body_response["uuid"]+
							"</Item>"+
						"</"+add_item_verb+"Request>" //VerifyAddFixedPriceItem //AddFixedPriceItem

	//fmt.Println(create_item_xml)
	return create_item_xml
}
