package stealerPkg

func Generate_xml_get(auction_id, token string)(string){
	get_auction_description_xml := "<?xml version='1.0' encoding='utf-8'?>"+
									"<GetItemRequest xmlns='urn:ebay:apis:eBLBaseComponents'>"+
										 "<DetailLevel>ItemReturnDescription</DetailLevel>"+
										 "<ErrorLanguage>en_US</ErrorLanguage>"+
										 "<WarningLevel>High</WarningLevel>"+
										 "<ItemID>"+auction_id+"</ItemID>"+
										"<IncludeItemSpecifics>True</IncludeItemSpecifics>"+
										 "<RequesterCredentials>"+
										 	"<eBayAuthToken>"+token+"</eBayAuthToken>"+
										 "</RequesterCredentials>"+
									"</GetItemRequest>"

	return get_auction_description_xml
}
