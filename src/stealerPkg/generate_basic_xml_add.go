package stealerPkg

func Generate_basic_xml_add(token string, ebay_item_body_response map[string]string, add_item_verb, auction_id string, picture_urls []string, paypal string)(string){

	create_item_xml := "<?xml version='1.0' encoding='utf-8'?>"+
						"<"+add_item_verb+"Request xmlns='urn:ebay:apis:eBLBaseComponents'>"+
							"<RequesterCredentials><eBayAuthToken>"+token+"</eBayAuthToken></RequesterCredentials>"+
							"<ErrorLanguage>en_US</ErrorLanguage>"+
							"<WarningLevel>High</WarningLevel>"+
							"<Item>"+
								"<Title>title of item "+auction_id+"</Title>"+ //german äöüßé //french èçëòôöùàâ//spanish ñ
								"<Description>description of item "+auction_id+"</Description>"+
								"<PrimaryCategory><CategoryID>"+ebay_item_body_response["category_id"]+"</CategoryID></PrimaryCategory>"+
								"<PrivateListing>False</PrivateListing>"+
								"<StartPrice currencyID='USD'>74.55</StartPrice>"+
								"<ConditionID>1000</ConditionID>"+
								"<CategoryMappingAllowed>true</CategoryMappingAllowed>"+
								"<Country>US</Country>"+
								"<Currency>USD</Currency>"+
								"<DispatchTimeMax>3</DispatchTimeMax>"+
								"<ListingDuration>Days_30</ListingDuration>"+
								"<ListingType>FixedPriceItem</ListingType>"+
								"<Location>US</Location>"+
								"<PaymentMethods>PayPal</PaymentMethods>"+
								"<PayPalEmailAddress>aprudivus@gmail.com</PayPalEmailAddress>"+
								"<Quantity>1</Quantity>"+
								"<ReturnPolicy><ReturnsAcceptedOption>ReturnsNotAccepted</ReturnsAcceptedOption></ReturnPolicy>"+
								"<ShippingDetails>"+
									"<ShippingType>Flat</ShippingType>"+
									"<ShippingServiceOptions>"+
										"<ShippingServicePriority>1</ShippingServicePriority>"+
										"<ShippingService>Pickup</ShippingService>"+
										"<ShippingServiceCost currencyID='USD'>0.0</ShippingServiceCost>"+
									"</ShippingServiceOptions>"+
								"</ShippingDetails>"+
								"<Site>US</Site>"+
								"<UUID>aaaaa00000bbbbb11111"+auction_id+"</UUID>"+
							"</Item>"+
						"</"+add_item_verb+"Request>"

	return create_item_xml
}
